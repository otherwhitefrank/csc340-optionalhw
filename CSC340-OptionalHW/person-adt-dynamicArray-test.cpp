/*
Name: person-adt-dynamicArray-test.cpp
Copyright: Frank Dye
Author: Frank Dye
Date: 11/19/13
Modified: 11/19/2013
Description: A simple test application for the Person ADT
*/

#include "Person.h"

int main()
{
	Person evan(11, "Evan", 7, 18);  //instantiate a Person object
	Person john(evan);//a copy constuctor will be automatically called

	john.update_name("John");
	john.add_email("john@gmail.com");
	john.add_email("john@yahoo.com");

	john.add_grade(30);
	john.add_grade(45);
	john.add_grade(55);
	cout << john;

	cout << "\nUpdating first email\n";
	john.update_email(0, "john@hotmail.com");
	cout << john;
	cout << "\nUpdating first grade\n";
	john.update_grade(0, 100);
	cout << john;

	cout << "\nRemoving first email\n";
	john.remove_email("john@hotmail.com");
	cout << john;

	cout << "\nRemoving first grade\n";
	john.remove_grade(0);
	cout << john;


	print_Person(evan); //a better way to test whether your copy constructor
	// is correctly implemented 

	evan = john;  //test the = operator
	evan.update_name("Evan");
	cout << evan;

	return 0;
}//end-of-main()