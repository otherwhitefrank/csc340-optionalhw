/*
Name: Person-adt-dynamicArray.cpp
Copyright: Frank Dye
Author: Frank Dye
Date: 11/19/2013
Modified: 11/19/2013
Description: Implementation of Person ADT
*/

#include "Person.h"


Person::~Person()
{
	if (email_list != NULL){
		delete [] email_list;
		email_list = NULL;
		num_emails = 0;
	}
	if (grade_list != NULL){
		delete [] grade_list;
		grade_list = NULL;
		num_grades = 0; //unnecessary
	}
}

//default constructor
Person::Person():id("0000"),name("na"),height(-1),age(-1),email_list(NULL),num_emails(0),grade_list(NULL),num_grades(0)
{
	//intentionally left empty
}

Person::Person(int init_id, string init_nm, double init_h, double init_age)
{
	id = to_string(init_id);
	name = init_nm;
	height = init_h;
	age = init_age;
	email_list = NULL;
	num_emails = 0;
	grade_list = NULL;
	num_grades = 0;
}

/*copy constructor*/
Person::Person(const Person & someone)
{
	id = someone.id;
	name = someone.name;
	height = someone.height;
	age = someone.age;
	num_emails = someone.num_emails;
	//allocate space to *email_list if num_emails>0
	if (num_emails <= 0)
		email_list = NULL;
	else //(num_emails > 0)
	{
		//need exception handling here 
		email_list = new string [num_emails];
		for (int i=0;i<num_emails; i++)
			email_list[ i ] = someone.email_list[ i ];
	}
	//*grades : similar to email_list
	num_grades = someone.num_grades;
	if (num_grades <= 0)
		grade_list = NULL;
	else //(num_grades > 0)
	{
		//need exception handling here 
		grade_list = new double [num_grades];
		for (int i=0;i<num_grades; i++)
			grade_list[ i ] = someone.grade_list[ i ];
	} 
}
/**/
/**/
Person Person::operator =(const Person & rhs)
{
	id = rhs.id;
	name = rhs.name;
	age = rhs.age;
	height = rhs.height;

	//allocate space to *emails if needed
	if (num_emails > 0)
		delete [] email_list;  //release the old memory
	num_emails = rhs.num_emails; //get the new
	if (num_emails <= 0)
		email_list = NULL;
	else{
		//need exception handling
		email_list = new string [num_emails];
		for (int i=0;i<num_emails; i++)
			email_list[ i ] = rhs.email_list[ i ];
	}
	//allocate space to grade_list
	if (num_grades > 0)
		delete [] grade_list;
	num_grades = rhs.num_grades;
	if (num_grades <= 0 )
		grade_list = NULL;
	else{
		//need exception handling
		try{
			grade_list = new double [num_grades];
		}
		catch(bad_alloc & e){
			cerr<< "Person::operator=(): "<< e.what();
		}
		for (int i=0; i<num_grades; i++)
			grade_list[ i] = rhs.grade_list[i];
	}     

	return (*this);
}

/**/
string Person::get_id() const
{
	return id;
}

string Person::get_name() const
{
	return (*this).name;//
}


double Person::get_height() const
{
	return height;
}

double Person::get_age() const
{
	return age;
}

int Person::get_num_emails() const
{
	return num_emails;
}

string Person::get_email( int i) const
{
	if (i>=0 && i<num_emails)
		return email_list[i];
	else
		return "NA";       
}

int Person::get_num_grades() const
{
	return num_grades;
}

double Person::get_grade( int i) const
{
	if (i>=0 && i<num_grades)
		return grade_list[i];
	else
		return -1.0;       
}

/**/
void Person::update_id(int new_id)
{
	char id_str[ID_LEN];
	sprintf( id_str, "%04d", new_id); //convert new_id to a c-string
	id = id_str;
}

void Person::update_name(string new_nm)
{
	name= new_nm;
}

void Person::update_height(double new_ht)
{
	height=new_ht;
}


void Person::update_age(double new_age)
{
	age=new_age;
}


void Person::add_email( string new_email)
{
	//case 1: emails list is empty
	if ( num_emails == 0){
		//need to handle exceptions try{} catch{}
		email_list = new string[1];
		email_list[0] = new_email;
	}
	else { //case 2: expand the list
		string *tmp_email_list = NULL;
		//exception handling
		try{
			tmp_email_list=new string [num_emails+1];
		}
		catch (bad_alloc& e){
			cerr<<"Person::add_email(): "<< e.what() ;
		}
		//move the emails to tmp_emails
		for (int i=0; i<num_emails; i++)  //save the emails
			tmp_email_list[i] = email_list[ i ];
		tmp_email_list[num_emails] = new_email;
		//release the space allocated to the old email_list
		delete [] email_list;
		email_list = tmp_email_list;
	}
	num_emails += 1;
}

//update the i-th email
void Person::update_email(int i, string new_email)
{
	if (i < num_emails)
	{
		//Update emails
		email_list[i] = new_email;
	}
	else
	{
		cerr << "Index out of bound for update_email\n";
	}

}

//remove the specified email address
void Person::remove_email(string email)
{
	//Find the index of the email
	int matchIndex = -1;
	for (int i = 0; i < num_emails; i++)
	{
		if (strcmp(email.c_str(), email_list[i].c_str()) == 0)
			matchIndex = 0;
	}

	if (matchIndex == -1)
	{
		//We didn't find the email
		cerr << "Email not found in remove_email!\n";
	}
	else
	{
		string* new_email_list = new string[num_emails-1];

		int j = 0; //We use this to keep track of copy index which is different then source
		for (int i = 0; i < num_emails; i++)
		{
			//Skip the email we are deleting
			if (i != matchIndex)
			{
				//copy old emails to new emails
				new_email_list[j] = email_list[i];
				//J should only be incremented when we copy, it is used to keep track of different indexes from source to destination array
				j++;
			}
		}

		//delete old email array
		delete [] email_list;
		//update pointer
		email_list = new_email_list;
		//decrement total number of emails
		num_emails--;
	}

}


//  add a new hw grade to *grades
void Person::add_grade(double hw_grd)
{
	double* new_grade_list = new double[num_grades + 1];

	for (int i = 0; i < num_grades;i++)
	{
		//Copy old list
		new_grade_list[i] = grade_list[i];
	}

	//copy new value in num_grades does not inclue 0 starting index so this works
	new_grade_list[num_grades] = hw_grd;

	//delete old array
	delete [] grade_list;

	num_grades++;

	grade_list = new_grade_list;
}

//modify the i-th grade to a new value
void Person::update_grade(int i, double new_val)
{
	if (i < num_grades)
	{
		//Update emails
		grade_list[i] = new_val;
	}
	else
	{
		cerr << "Index out of bound for update_grade\n";
	}


}

//remove the i-th grade
void Person::remove_grade(int removeIndex)
{
	if (removeIndex < num_grades)
	{
		double* new_grade_list = new double[num_grades-1];

		int j = 0;
		for (int i = 0; i < num_grades; i++)
		{
			//Skip the email we are deleting
			if (i != removeIndex)
			{
				//copy old emails to new emails
				new_grade_list[j] = grade_list[i];
				//J should only be incremented when we copy, it is used to keep track of different indexes from source to destination array
				j++;
			}
		}

		//delete old email array
		delete [] grade_list;
		//update pointer
		grade_list = new_grade_list;
		//decrement total number of emails
		num_grades--;
	}
	else
	{
		//We didn't find the email
		cerr << "grade not found in remove_grade!\n";
	}
}

/**/
void Person::print() const
{
	cout << "(id="      << id ;
	cout << " name="    <<name;
	cout << " height="  << height;
	cout << " age="    << age <<")"<< endl;
	for (int i=0; i<num_emails; i++)
		cout <<" Email account #" << i
		<< " :" << email_list[ i ] <<endl;
}//
/**/

ostream & operator<<( ostream& output, const Person & someone)
{
	output << "(id="      << someone.id ;
	output << " name="    <<someone.name;
	output << " height="  << someone.height;
	output << " age="    << someone.age;
	output << " num_emails=" << someone.num_emails;
	output << " num_grades=" << someone.num_grades<< ")"<< endl;
	for (int i=0; i<someone.num_emails; i++)
		output <<" Email account #" << i
		<< " :" << someone.email_list[ i ] <<endl;
	for (int i=0; i<someone.num_grades; i++)
		output <<" Grade #" << i
		<< " :" << someone.grade_list[ i ] <<endl;
	return output;
}//

Person print_Person(Person someone)
{
	int num_emails = someone.get_num_emails();
	int num_grades = someone.get_num_grades();
	cout << "(id="      << someone.get_id() ;
	cout << " name="    << someone.get_name();
	cout << " height="  << someone.get_height();
	cout << " age="    << someone.get_age() ;
	cout << " num_emails=" << num_emails;
	cout << " num_grades=" << num_grades<< ")"<< endl;

	for (int i=0; i<num_emails; i++)
		cout <<" Email account #" << i
		<< " :" << someone.get_email( i ) <<endl;
	for (int i=0; i<num_grades; i++)
		cout <<" Grade #" << i
		<< " :" << someone.get_grade( i ) <<endl;
	return someone;
}//

/////////////////////////
//////////////////////////