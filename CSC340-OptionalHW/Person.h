/*
Name: Person.h
Copyright: Frank Dye
Author: Frank Dye
Date: 11/19/2013
Modified: 11/19/2013

Description: Definition of Person ADT
*/
#ifndef _PERSON_ADT_H
#define _PERSON_ADT_H

#include <iostream>
#include <string>
#include <exception>
#include <stdlib.h>

using namespace std;

#define ID_LEN 20  //maximum length of the person ID field

class Student;

//create a new data type Person using class
class Person {


public:

	//destuctor: to release any memory that has been allocated to the object
	~Person();

	Person(); //default constructor: (0, "na", -1, -1)
	Person(int init_id, string init_nm, double init_h, double init_age); //id0->id, name0->name, ...
	Person( const Person & someone);

	Person operator= (const Person & rhs);//assing rhs to the calling object

	//accessors
	string get_id() const; //return id
	string get_name() const; //return name;
	double get_height() const;//return height;
	double get_age() const;//return age;
	int    get_num_emails() const; //return num_emails
	string get_email( int i) const; //return the i-th email if exists; o.w: return "NA"
	int    get_num_grades() const; //return num_grades               
	double get_grade(int i) const; //get the i-th grade if exists; o.w: return "-1"

	//mutators
	void update_id( int new_id); //new_id --> id
	void update_name(string new_nm); //new_nm-->name
	void update_height(double new_ht);//new_ht-->height
	void update_age(double new_age); //new_age --> age
	void add_email( string the_email); //add the_email to the list
	void update_email(int i, string new_email); //update the i-th email
	void remove_email(string email); //remove the specified email address
	void add_grade( double hw_grd  ); //  add a new hw grade to *grades
	void update_grade(int i, double new_val); //modify the i-th grade to a new value
	void remove_grade(int removeIndex); //remove the i-th grade

	//other member fuctions
	void print() const; //print out the value of each data member

	//print out every data member of the object someone to output
	friend ostream & operator<<( ostream& output, const Person & someone);

private:
	string id;  //identification number
	string name; //the full name of a person

	double height; //
	double age; //
	string *email_list;//
	int    num_emails; //size of *email_list
	double *grade_list; //all the hw grades
	int    num_grades; //

};


Person print_Person(Person someone); //print out the information of a person


#endif //